
# checks for existing targets before adding extern dependencies.
# tries to handle transitive dependencies safer together with submodules
#
# usage:
# add_dependency(your_executable extern_folder_name dependency_name)
#
#
macro(add_dependency executable folder dependency)

    if (TARGET ${dependency})
        massage(STATUS "[${executable}] already contains [${dependency}] - please check transitive dependencies.")
    else()
        add_subdirectory(${folder}/${dependency})
        set_property(TARGET ${dependency} PROPERTY FOLDER "${folder}")
        message(STATUS "[${executable}] added [${dependency}]")
    endif()
    
endmacro()