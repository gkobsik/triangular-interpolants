#include "doctest.hh"
#include "utils/GeometryFunc.hh"

TEST_SUITE("GeometryFunc")
{

    TEST_CASE("computeHeight3Point") 
    {
        SUBCASE("Example1") {
            float height = GeometryFunc::computeHeight3Point(
                tg::pos3(2, 0, 6),
                tg::pos3(-7, 0, 4),
                tg::pos3(2, 0, 1),
                tg::pos3(1, 0, 1));
            CHECK(height == doctest::Approx(0.0f));
        }

        SUBCASE("Example2") {
            float height = GeometryFunc::computeHeight3Point(
                tg::pos3(0, 3, 6),
                tg::pos3(3, 0, 1),
                tg::pos3(2, 1, 5),
                tg::pos3(1, 0, 7));
            CHECK(height == doctest::Approx(2.0f));
        }
    
    }

    TEST_CASE("computeHeightPointNormal") 
    {
        SUBCASE("Example1") {
            float height = GeometryFunc::computeHeightPointNormal(
                tg::pos3(0, 1, 0),
                tg::vec3(0, 1, 0),
                tg::pos3(3, 0, 4));
            CHECK(height == doctest::Approx(1.0f));
        }

        SUBCASE("Example2") {
            float height = GeometryFunc::computeHeightPointNormal(
                tg::pos3(2, 1, 0),
                tg::vec3(0.70710678118, 0.70710678118, 0),
                tg::pos3(2, 0, 7));
            CHECK(height == doctest::Approx(1.0f));
        }

    }

    TEST_CASE("computeBarycentricCoords") 
    {
        SUBCASE("Example1") {
            tg::vec2 uv = GeometryFunc::computeBarycentricCoords(
                tg::pos2(2.5, 1),
                tg::pos2(1, 4),
                tg::pos2(1, 1),
                tg::pos2(4, 1));
            CHECK(uv[0] == doctest::Approx(0.0f));
            CHECK(uv[1] == doctest::Approx(0.5f));
        }

        SUBCASE("Example2") {
            tg::vec2 uv = GeometryFunc::computeBarycentricCoords(
                tg::pos2(3, 3),
                tg::pos2(3, 5),
                tg::pos2(1, 2),
                tg::pos2(6, 2));
            CHECK(uv[0] == doctest::Approx(0.333333333f));
        }
 
    }

} //end "GeometryFunc"