#include "doctest.hh"
#include "utils/DeCasteljauAlgorithm.hh"


TEST_SUITE("DeCasteljauAlgorithm")
{
    TEST_CASE("createTriangleBezierIndizies - NumberOfIndizies")
    {
        CHECK(1 == DeCasteljauAlgorithm::triangleBezierIndizies(0).size());
        CHECK(3 == DeCasteljauAlgorithm::triangleBezierIndizies(1).size());
        CHECK(6 == DeCasteljauAlgorithm::triangleBezierIndizies(2).size());
        CHECK(10 == DeCasteljauAlgorithm::triangleBezierIndizies(3).size());
        CHECK(15 == DeCasteljauAlgorithm::triangleBezierIndizies(4).size());
    }

    TEST_CASE("deCasteljauTriangle")
    {
        SUBCASE("Example1")
        {
            auto controlPoints = DeCasteljauAlgorithm::triangleBezierIndizies(2);
            controlPoints[tg::uvec3(2, 0, 0)] = tg::vec3(0, 0, 0);
            controlPoints[tg::uvec3(1, 1, 0)] = tg::vec3(1, 0, 2);
            controlPoints[tg::uvec3(0, 2, 0)] = tg::vec3(0, 2, 0);
            controlPoints[tg::uvec3(1, 0, 1)] = tg::vec3(0, 1, 0);
            controlPoints[tg::uvec3(0, 1, 1)] = tg::vec3(1, 1, 1);
            controlPoints[tg::uvec3(0, 0, 2)] = tg::vec3(0, 2, 1);

            tg::vec2 uv = tg::vec2(0.5, 0.3333333333);
            tg::pos3 pos = DeCasteljauAlgorithm::evaluateTriangle(controlPoints, 2, uv);
            tg::vec3 expected_pos = tg::vec3(4. / 9., 5. / 9., 29. / 36.);

            CHECK(expected_pos.x == doctest::Approx(pos.x));
            CHECK(expected_pos.y == doctest::Approx(pos.y));
            CHECK(expected_pos.z == doctest::Approx(pos.z));
        }    
    }
} //end "DeCasteljauAlgorithm"
