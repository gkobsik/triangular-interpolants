#include "doctest.hh"
#include "CloughTocherInterpolant.hh"
#include "geometry/TriMeshUtils.hh"

#include <iostream>
#include <fstream>
#include <string>
#include <functional>

TEST_CASE("computeControlPointsXZPositions - Example1")
{
    auto mesh = std::make_shared<TriMeshCT>();

    std::vector<pm::vertex_handle> vh(3);
    for (int i = 0; i < 3; ++i)
        vh[i] = mesh->vertices().add();

    mesh->position[vh[0]] = tg::pos3(0, 0, 0);
    mesh->position[vh[1]] = tg::pos3(3, 0, 0);
    mesh->position[vh[2]] = tg::pos3(0, 0, 3);
    mesh->faces().add(vh[0], vh[1], vh[2]);

    auto ctInterpolant = CloughTocherInterpolant(mesh);
    ctInterpolant.computeControlPointXZPositions();

    mesh = ctInterpolant.getMesh();

    auto S_pos = mesh->S[mesh->faces().first()].pos;
    CHECK(S_pos.x == doctest::Approx(1.0f));
    CHECK(S_pos.y == doctest::Approx(1.0f));

    auto V0_pos = mesh->V[vh[0]].pos;
    CHECK(V0_pos.x == doctest::Approx(0.0f));
    CHECK(V0_pos.y == doctest::Approx(0.0f));

    auto V1_pos = mesh->V[vh[1]].pos;
    CHECK(V1_pos.x == doctest::Approx(3.0f));
    CHECK(V1_pos.y == doctest::Approx(0.0f));

    auto V2_pos = mesh->V[vh[2]].pos;
    CHECK(V2_pos.x == doctest::Approx(0.0f));
    CHECK(V2_pos.y == doctest::Approx(3.0f));

    for (auto hh : mesh->halfedges())
    {
        auto toVertexPos = mesh->V[hh.vertex_to()].pos;
        auto fromVertexPos = mesh->V[hh.vertex_from()].pos;

        if (fromVertexPos == tg::pos2(0, 0) && toVertexPos == tg::pos2(3, 0))
        {
            auto T_pos = mesh->T[hh].pos;
            CHECK(T_pos.x == doctest::Approx(1.0f));
            CHECK(T_pos.y == doctest::Approx(0.0f));

            auto C_pos = mesh->C[hh].pos;
            CHECK(C_pos.x == doctest::Approx(1.33333333f));
            CHECK(C_pos.y == doctest::Approx(0.33333333f));

            auto I1_pos = mesh->I1[hh].pos;
            CHECK(I1_pos.x == doctest::Approx(0.33333333f));
            CHECK(I1_pos.y == doctest::Approx(0.33333333f));

            auto I2_pos = mesh->I2[hh].pos;
            CHECK(I2_pos.x == doctest::Approx(0.66666666f));
            CHECK(I2_pos.y == doctest::Approx(0.66666666f));
        }
        else if (fromVertexPos == tg::pos2(3, 0) && toVertexPos == tg::pos2(0, 3))
        {
            auto T_pos = mesh->T[hh].pos;
            CHECK(T_pos.x == doctest::Approx(2.0f));
            CHECK(T_pos.y == doctest::Approx(1.0f));

            auto C_pos = mesh->C[hh].pos;
            CHECK(C_pos.x == doctest::Approx(1.33333333f));
            CHECK(C_pos.y == doctest::Approx(1.33333333f));

            auto I1_pos = mesh->I1[hh].pos;
            CHECK(I1_pos.x == doctest::Approx(2.33333333f));
            CHECK(I1_pos.y == doctest::Approx(0.33333333f));

            auto I2_pos = mesh->I2[hh].pos;
            CHECK(I2_pos.x == doctest::Approx(1.66666666f));
            CHECK(I2_pos.y == doctest::Approx(0.66666666f));
        }
        else if (fromVertexPos == tg::pos2(0, 3) && toVertexPos == tg::pos2(0, 0))
        {
            auto T_pos = mesh->T[hh].pos;
            CHECK(T_pos.x == doctest::Approx(0.0f));
            CHECK(T_pos.y == doctest::Approx(2.0f));

            auto C_pos = mesh->C[hh].pos;
            CHECK(C_pos.x == doctest::Approx(0.33333333f));
            CHECK(C_pos.y == doctest::Approx(1.33333333f));

            auto I1_pos = mesh->I1[hh].pos;
            CHECK(I1_pos.x == doctest::Approx(0.33333333f));
            CHECK(I1_pos.y == doctest::Approx(2.33333333f));

            auto I2_pos = mesh->I2[hh].pos;
            CHECK(I2_pos.x == doctest::Approx(0.66666666f));
            CHECK(I2_pos.y == doctest::Approx(1.66666666f));
        }
        else if (fromVertexPos == tg::pos2(3, 0) && toVertexPos == tg::pos2(0, 0))
        {
            auto T_pos = mesh->T[hh].pos;
            CHECK(T_pos.x == doctest::Approx(2.0f));
            CHECK(T_pos.y == doctest::Approx(0.0f));

            auto C_pos = mesh->C[hh].pos;
            CHECK(C_pos.x == doctest::Approx(0.0f));
            CHECK(C_pos.y == doctest::Approx(0.0f));

            auto I1_pos = mesh->I1[hh].pos;
            CHECK(I1_pos.x == doctest::Approx(0.0f));
            CHECK(I1_pos.y == doctest::Approx(0.0f));

            auto I2_pos = mesh->I2[hh].pos;
            CHECK(I2_pos.x == doctest::Approx(0.0f));
            CHECK(I2_pos.y == doctest::Approx(0.0f));
        }
        else if (fromVertexPos == tg::pos2(0, 3) && toVertexPos == tg::pos2(3, 0))
        {
            auto T_pos = mesh->T[hh].pos;
            CHECK(T_pos.x == doctest::Approx(1.0f));
            CHECK(T_pos.y == doctest::Approx(2.0f));

            auto C_pos = mesh->C[hh].pos;
            CHECK(C_pos.x == doctest::Approx(0.0f));
            CHECK(C_pos.y == doctest::Approx(0.0f));

            auto I1_pos = mesh->I1[hh].pos;
            CHECK(I1_pos.x == doctest::Approx(0.0f));
            CHECK(I1_pos.y == doctest::Approx(0.0f));

            auto I2_pos = mesh->I2[hh].pos;
            CHECK(I2_pos.x == doctest::Approx(0.0f));
            CHECK(I2_pos.y == doctest::Approx(0.0f));
        }
        else if (fromVertexPos == tg::pos2(0, 0) && toVertexPos == tg::pos2(0, 3))
        {
            auto T_pos = mesh->T[hh].pos;
            CHECK(T_pos.x == doctest::Approx(0.0f));
            CHECK(T_pos.y == doctest::Approx(1.0f));

            auto C_pos = mesh->C[hh].pos;
            CHECK(C_pos.x == doctest::Approx(0.0f));
            CHECK(C_pos.y == doctest::Approx(0.0f));

            auto I1_pos = mesh->I1[hh].pos;
            CHECK(I1_pos.x == doctest::Approx(0.0f));
            CHECK(I1_pos.y == doctest::Approx(0.0f));

            auto I2_pos = mesh->I2[hh].pos;
            CHECK(I2_pos.x == doctest::Approx(0.0f));
            CHECK(I2_pos.y == doctest::Approx(0.0f));
        }
    }
}

TEST_CASE("computeControlPointsXZPositions - Example2")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(5, 5);
    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    auto mesh = ctInterpolant.getMesh();

    for (auto vh : mesh->vertices())
    {
        tg::pos2 pos = mesh->V[vh].pos;
        CHECK(pos.x >= 0.0f);
        CHECK(pos.y >= 0.0f);
    }
}

TEST_CASE("CT_step1 - Example1")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(1, 1);    
    std::vector<std::vector<float>> values{
        {0,3}, {0,0},
        {0,0}, {1,0} };
    auto ctInterpolant = CloughTocherInterpolant(plane);

    ctInterpolant.computeControlPointXZPositions();
    ctInterpolant.CT_step1(values);
    auto mesh = ctInterpolant.getMesh();

    for (auto vh : mesh->vertices())
    {
        tg::pos2 pos = mesh->V[vh].pos;
        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
        {
            float val = mesh->V[vh].val[i];

            if (pos == tg::pos2(0, 0))
            {
                if (i == 0) {
                    CHECK(val == doctest::Approx(0.0f));
                }
                else if (i == 1) {
                    CHECK(val == doctest::Approx(3.0f));
                }
            }
            else if (pos == tg::pos2(0, 3)) {
                if (i == 0) {
                    CHECK(val == doctest::Approx(0.0f));
                }
                else if (i == 1) {
                    CHECK(val == doctest::Approx(0.0f));
                }
            }
            else if (pos == tg::pos2(3, 0)) {
                if (i == 0) {
                    CHECK(val == doctest::Approx(0.0f));
                }
                else if (i == 1) {
                    CHECK(val == doctest::Approx(0.0f));
                }
            }
            else if (pos == tg::pos2(3, 3)) {
                if (i == 0) {
                    CHECK(val == doctest::Approx(1.0f));
                }
                else if (i == 1) {
                    CHECK(val == doctest::Approx(0.0f));
                }
            }
        }
    }
}

TEST_CASE("CT_step1 - Example2")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(3, 3);

    std::vector<std::vector<float>> values{
        {6,1}, {0,0}, {4,7}, {3,4},
        {0,1}, {1,3}, {0,1}, {3,0},
        {2,1}, {2,4}, {3,4}, {0,1},
        {0,1}, {5,3}, {5,2}, {3,0} };

    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    ctInterpolant.CT_step1(values);
    auto mesh = ctInterpolant.getMesh();

    for (auto vh : mesh->vertices())
    {
        tg::pos2 pos = mesh->V[vh].pos;
        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
        {
            float val = mesh->V[vh].val[i];

            if (pos == tg::pos2(0, 0))
            {
                if (i == 0) {
                    CHECK(val == doctest::Approx(6.0f));
                }
                else if (i == 1) {
                    CHECK(val == doctest::Approx(1.0f));
                }
            }
            else if (pos == tg::pos2(3, 6)) {
                if (i == 0) {
                    CHECK(val == doctest::Approx(2.0f));
                }
                else if (i == 1) {
                    CHECK(val == doctest::Approx(4.0f));
                }
            }
            else if (pos == tg::pos2(9, 3)) {
                if (i == 0) {
                    CHECK(val == doctest::Approx(3.0f));
                }
                else if (i == 1) {
                    CHECK(val == doctest::Approx(0.0f));
                }
            }
            else if (pos == tg::pos2(6, 9)) {
                if (i == 0) {
                    CHECK(val == doctest::Approx(5.0f));
                }
                else if (i == 1) {
                    CHECK(val == doctest::Approx(2.0f));
                }
            }
        }
    }
}

TEST_CASE("computeNormals-  Example1")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(2, 2);
    std::vector<std::vector<float>> values{
        {0,0}, {0,0}, {0,0},
        {0,0}, {0,0}, {0,0},
        {0,0}, {0,0}, {0,0} };

    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    ctInterpolant.CT_step1(values);
    ctInterpolant.computeNormals();
    auto mesh = ctInterpolant.getMesh();

    for (auto vh : mesh->vertices())
    {
        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
        {
            tg::vec3 normal = mesh->N[vh].normal[i];
            CHECK(normal.x == doctest::Approx(0.0f));
            CHECK(normal.y == doctest::Approx(1.0f));
            CHECK(normal.z == doctest::Approx(0.0f));
        }
    }
}

TEST_CASE("computeNormals - Example2")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(1, 1);
    std::vector<std::vector<float>> values{
        {3,3}, {3,0},
        {0,0}, {0,0} };

    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    ctInterpolant.CT_step1(values);
    ctInterpolant.computeNormals();
    auto mesh = ctInterpolant.getMesh();


    for (auto vh : mesh->vertices())
    {
        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
        {
            tg::vec3 normal = mesh->N[vh].normal[i];
            tg::pos2 pos = mesh->V[vh].pos;
            if (i == 0) {
                CHECK(normal.x == doctest::Approx(0.0f).epsilon(0.001));
                CHECK(normal.y == doctest::Approx(0.7071068f).epsilon(0.001));
                CHECK(normal.z == doctest::Approx(0.7071068f).epsilon(0.001));
            }
            else if (i == 1)
            {
                if (pos == tg::pos2(0, 0))
                {
                    CHECK(normal.x >= 0.55);
                    CHECK(normal.y >= 0.55);
                    CHECK(normal.z >= 0.55);
                }
                else if (pos == tg::pos2(0, 3))
                {
                    CHECK(normal.x >= 0.45);
                    CHECK(normal.y >= 0.75);
                    CHECK(normal.z >= 0.45);
                }
                else if (pos == tg::pos2(3, 0))
                {
                    CHECK(normal.x >= 0.45);
                    CHECK(normal.y >= 0.75);
                    CHECK(normal.z >= 0.45);
                }
                else if (pos == tg::pos2(3, 3))
                {
                    CHECK(normal.x == doctest::Approx(0.0));
                    CHECK(normal.y == doctest::Approx(1.0));
                    CHECK(normal.z == doctest::Approx(0.0));
                }
            }
        }
    }
}

TEST_CASE("computeNormals - Example3")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(1, 1);
    std::vector<std::vector<float>> values{
        {0, 0}, {3, 0},
        {0, 0}, {0, 0} };

    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    ctInterpolant.CT_step1(values);
    ctInterpolant.computeNormals();
    auto mesh = ctInterpolant.getMesh();
    for (auto vh : mesh->vertices())
    {
        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
        {
            tg::vec3 normal = plane->N[vh].normal[i];
            tg::pos2 pos = plane->V[vh].pos;

            if (i == 0)
            {
                if (pos == tg::pos2(0, 0))
                {
                    CHECK(normal.x == doctest::Approx(-0.7071068f).epsilon(0.001));
                    CHECK(normal.y == doctest::Approx(0.7071068f).epsilon(0.001));
                    CHECK(normal.z == doctest::Approx(0.0f).epsilon(0.001));
                }
                else if (pos == tg::pos2(0, 3))
                {
                    CHECK(normal.x <= 0.45);
                    CHECK(normal.y >= 0.76);
                    CHECK(normal.z >= 0.45);
                }
                else if (pos == tg::pos2(3, 0))
                {
                    CHECK(normal.x <= 0.45);
                    CHECK(normal.y >= 0.76);
                    CHECK(normal.z >= 0.45);
                }
                else if (pos == tg::pos2(3, 3))
                {
                    CHECK(normal.x == doctest::Approx(0.0f).epsilon(0.001));
                    CHECK(normal.y == doctest::Approx(0.7071068f).epsilon(0.001));
                    CHECK(normal.z == doctest::Approx(0.7071068f).epsilon(0.001));
                }
            }
        }
    }
}

struct heighFieldFunctionStruct {
    std::string name;
    std::function<float(const tg::pos2&)> heightFieldFunction;
    tg::vec2 planeSize;
    tg::vec2 numberOfSamples;
    float precision;
    bool randomSamples;
    bool outputObj;
    bool compareHeight;
    bool padding;
};

static std::vector<tg::vec3> 
initializeMesh(
    std::shared_ptr<TriMeshCT> mesh, 
    heighFieldFunctionStruct inputParams
)
{
    std::vector<tg::vec3> points;
    auto height_function = inputParams.heightFieldFunction;

    for (auto vh : mesh->vertices())
    {
        auto V_pos = mesh->V[vh].pos;
        mesh->V[vh].val[0] =
            height_function(V_pos);
        auto V_val = mesh->V[vh].val;
        points.push_back(tg::vec3(V_pos[0], V_val[0], V_pos[1]));
    }

    for (auto fh : mesh->faces())
    {
        auto S_pos = mesh->S[fh].pos;
        mesh->S[fh].val[0] =
            height_function(S_pos);
        auto S_val = mesh->S[fh].val;
        points.push_back(tg::vec3(S_pos[0], S_val[0], S_pos[1]));
    }

    for (auto hh : mesh->halfedges())
    {
        auto T_pos = mesh->T[hh].pos;
        mesh->T[hh].val[0] =
            height_function(T_pos);
        auto T_val = mesh->T[hh].val;
        points.push_back(tg::vec3(T_pos[0], T_val[0], T_pos[1]));

        if (hh.face().is_valid())
        {
            auto C_pos = mesh->C[hh].pos;
            mesh->C[hh].val[0] =
                height_function(C_pos);
            auto C_val = mesh->C[hh].val;
            points.push_back(tg::vec3(C_pos[0], C_val[0], C_pos[1]));

            auto I1_pos = mesh->I1[hh].pos;
            mesh->I1[hh].val[0] =
                height_function(I1_pos);
            auto I1_val = mesh->I1[hh].val;
            points.push_back(tg::vec3(I1_pos[0], I1_val[0], I1_pos[1]));

            auto I2_pos = mesh->I2[hh].pos;
            mesh->I2[hh].val[0] =
                height_function(I2_pos);
            auto I2_val = mesh->I2[hh].val;
            points.push_back(tg::vec3(I2_pos[0], I2_val[0], I2_pos[1]));
        }
    }

    return points;
}

static void 
evaluateInterpolant(
    CloughTocherInterpolant ctInterpolant, 
    std::vector<tg::vec3> points,
    heighFieldFunctionStruct inputParams
)
{
    auto height_function = inputParams.heightFieldFunction;

    std::ofstream myfile;

    if (inputParams.outputObj)
    {
        
        myfile.open("test-out/" + inputParams.name + ".obj", std::ios::trunc);

        myfile << "o CloughTocherInterpolation" << std::endl;
        myfile << "g ControlPoints" << std::endl;
        for (int i = 0; i < points.size(); ++i)
        {
            myfile << "v " + std::to_string(points[i][0]) + " " + std::to_string(points[i][1]) + " " + std::to_string(points[i][2]) << std::endl;
        }
        myfile << "g Evaluated Heights" << std::endl;
    }

    if (inputParams.randomSamples)
    {
        for (int j = 0; j <= inputParams.numberOfSamples[0] * inputParams.numberOfSamples[1]; ++j) {
            tg::pos2 pos = tg::pos2(
                (static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX)) * 3.0f * inputParams.planeSize[0],
                (static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX)) * 3.0f * inputParams.planeSize[1]);
            float value = ctInterpolant.getValueAt(pos);
            if (inputParams.outputObj)
                myfile << "v " + std::to_string(pos[0]) + " " + std::to_string(value) + " " + std::to_string(pos[1]) << std::endl;
            if (inputParams.compareHeight)
                CHECK(abs(height_function(pos) - value) <= inputParams.precision);
        }
    }
    else
    {
        for (int i = 0; i <= inputParams.numberOfSamples[0]; ++i)
            for (int j = 0; j <= inputParams.numberOfSamples[1]; ++j) {
                tg::pos2 pos = tg::pos2(i / (inputParams.numberOfSamples[0] / 3.0f), j / (inputParams.numberOfSamples[1] / 3.0f));
                float value = ctInterpolant.getValueAt(pos);
                if (inputParams.outputObj)
                    myfile << "v " + std::to_string(pos[0]) + " " + std::to_string(value) + " " + std::to_string(pos[1]) << std::endl;
                if (inputParams.compareHeight)
                    CHECK(abs(height_function(pos) - value) <= inputParams.precision);
            }
    }

    if (inputParams.outputObj)
    {
        myfile.close();
    }
}

TEST_CASE("heightFieldFunction - oneThirdSquaredX")
{
    heighFieldFunctionStruct inputParams{
        "heightFieldFunction - oneThirdSquaredX", //name
        [](tg::pos2 pnt)
        {
            return (pnt[0] / 3.0f) * (pnt[0] / 3.0f);
        }, //heightFieldFunction
        tg::vec2(1,1), //planeSize
        tg::vec2(100, 100), //numberOfSamples
        0.00001f, //precision
        false, //randomSamples
        true, //outputObj
        false, //compareHeight
        false
    };

    auto plane = TriMeshUtils::createSimplePlaneCT(
        static_cast<int>(inputParams.planeSize[0]),
        static_cast<int>(inputParams.planeSize[1])
    );

    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    auto mesh = ctInterpolant.getMesh();

    auto points = initializeMesh(mesh, inputParams);
    evaluateInterpolant(ctInterpolant, points, inputParams);    
}

TEST_CASE("evaluateBezierPatchAt - tiltedPlane")
{
    heighFieldFunctionStruct inputParams{
        "evaluateBezierPatchAt - tiltedPlane", //name
        [](tg::pos2 pnt)
        {
            return (pnt[0] / 3.0f) - (pnt[1] / 3.0f);
        }, //heightFieldFunction
        tg::vec2(1,1), //planeSize
        tg::vec2(50, 50), //numberOfSamples
        0.0001f, //precision
        false, //randomSamples
        true, //outputObj
        false, //compareHeight
        false
    };

    auto plane = TriMeshUtils::createSimplePlaneCT(
        static_cast<int>(inputParams.planeSize[0]),
        static_cast<int>(inputParams.planeSize[1])
    );

    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    auto mesh = ctInterpolant.getMesh();

    auto points = initializeMesh(mesh, inputParams);
    evaluateInterpolant(ctInterpolant, points, inputParams);
}

TEST_CASE("evaluateBezierPatchAt - squaredFunction")
{
    heighFieldFunctionStruct inputParams{
        "evaluateBezierPatchAt - squaredFunction", //name
        [](tg::pos2 pnt)
        {
            return (pnt[0] / 3.0f) * (pnt[0] / 3.0f);
        }, //heightFieldFunction
        tg::vec2(1,1), //planeSize
        tg::vec2(50, 50), //numberOfSamples
        0.00001f, //precision
        false, //randomSamples
        true, //outputObj
        false, //compareHeight
        false
    };

    auto plane = TriMeshUtils::createSimplePlaneCT(
        static_cast<int>(inputParams.planeSize[0]),
        static_cast<int>(inputParams.planeSize[1])
    );

    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    auto mesh = ctInterpolant.getMesh();

    auto points = initializeMesh(mesh, inputParams);
    evaluateInterpolant(ctInterpolant, points, inputParams);
}

TEST_CASE("evaluateBezierPatchAt - squaredFunction2")
{
    heighFieldFunctionStruct inputParams{
        "evaluateBezierPatchAt - squaredFunction2", //name
        [](tg::pos2 pnt)
        {
            return (pnt[0] / 3.0f) * (pnt[0] / 3.0f);
        }, //heightFieldFunction
        tg::vec2(1,1), //planeSize
        tg::vec2(50, 50), //numberOfSamples
        0.00001f, //precision
        false, //randomSamples
        true, //outputObj
        false, //compareHeight
        false
    };

    auto plane = TriMeshUtils::createSimplePlaneCT(
        static_cast<int>(inputParams.planeSize[0]),
        static_cast<int>(inputParams.planeSize[1])
    );

    auto ctInterpolant = CloughTocherInterpolant(plane);
    ctInterpolant.computeControlPointXZPositions();
    auto mesh = ctInterpolant.getMesh();

    auto points = initializeMesh(mesh, inputParams);
    evaluateInterpolant(ctInterpolant, points, inputParams);
}

static void
outputOBJpoints(CloughTocherInterpolant ctInterpolant, std::string outputFile = "objOutput", int index = 0)
{
    auto mesh = ctInterpolant.getMesh();

    std::vector<tg::pos3> points;

    for (auto vh : mesh->vertices())
    {
        auto const V_pos = mesh->V[vh].pos;
        auto const V_val = mesh->V[vh].val;
        points.push_back(tg::pos3(V_pos[0], V_val[index], V_pos[1]));
    }

    for (auto fh : mesh->faces())
    {
        auto const S_pos = mesh->S[fh].pos;
        auto const S_val = mesh->S[fh].val;
        points.push_back(tg::pos3(S_pos[0], S_val[index], S_pos[1]));
    }

    for (auto hh : mesh->halfedges())
    {
        auto T_pos = mesh->T[hh].pos;
        auto T_val = mesh->T[hh].val;
        points.push_back(tg::pos3(T_pos[0], T_val[index], T_pos[1]));

        if (hh.face().is_valid())
        {
            auto C_pos = mesh->C[hh].pos;
            auto C_val = mesh->C[hh].val;
            points.push_back(tg::pos3(C_pos[0], C_val[index], C_pos[1]));

            auto I1_pos = mesh->I1[hh].pos;
            auto I1_val = mesh->I1[hh].val;
            points.push_back(tg::pos3(I1_pos[0], I1_val[index], I1_pos[1]));

            auto I2_pos = mesh->I2[hh].pos;
            auto I2_val = mesh->I2[hh].val;
            points.push_back(tg::pos3(I2_pos[0], I2_val[index], I2_pos[1]));
        }
    }

    std::ofstream myfile;
    myfile.open("test-out/" + outputFile + ".obj", std::ios::trunc);
    myfile << "o CloughTocherInterpolation" << '\n';

    myfile << "g ControlPoints" << '\n';
    for (long unsigned int i = 0; i < points.size(); ++i)
    {
        std::string output = "v " + std::to_string(points[i][0]) + " " + std::to_string(points[i][1]) + " " + std::to_string(points[i][2]);
        myfile << output.c_str() << '\n';
    }

    myfile << "g Evaluated Heights" << '\n';
    for (int i = -10; i < 100; ++i)
        for (int j = -10; j < 100; ++j) {
            tg::pos2 pos = tg::pos2((i + 0.5f) / 10.0f, (j + 0.5f) / 10.f);
            float value = ctInterpolant.getValueAt(pos);
            std::string output = "v " + std::to_string(pos[0]) + " " + std::to_string(value) + " " + std::to_string(pos[1]);
            myfile << output.c_str() << '\n';
        }

    myfile.close();
}

TEST_CASE("computeCloughTocherInterpolant - Example1")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(2, 2);

    std::vector<std::vector<float>> values{
        {1,1}, {3,0}, {0,0},
        {0,0}, {2,1}, {0,0},
        {0,0}, {2,1}, {0,0}
    };

    auto ctInterpolant = CloughTocherInterpolant(plane, values);
    outputOBJpoints(ctInterpolant, "computeCloughTocherInterpolant - Example1", 0);

}

TEST_CASE("computeCloughTocherInterpolant - Example2")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(3, 3);

    std::vector<std::vector<float>> values{
        {1,1}, {2,0}, {2,7}, {4,4},
        {1,1}, {4,3}, {1,1}, {2,0},
        {0,1}, {1,4}, {2,4}, {0,1},
        {1,1}, {2,3}, {3,2}, {0,0} };

    auto ctInterpolant = CloughTocherInterpolant(plane, values);
    outputOBJpoints(ctInterpolant, "computeCloughTocherInterpolant - Example2", 0);
}

TEST_CASE("computeCloughTocherInterpolant - Example3")
{
    auto plane = TriMeshUtils::createSimplePlaneCT(3, 3);

    std::vector<std::vector<float>> values{
        {0,0}, {0,0}, {0,0}, {0,0},
        {0,0}, {0,0}, {0,0}, {0,0},
        {0,0}, {0,0}, {0,0}, {0,0},
        {0,0}, {0,0}, {0,0}, {0,0} };

    auto ctInterpolant = CloughTocherInterpolant(plane, values);
    outputOBJpoints(ctInterpolant, "computeCloughTocherInterpolant - Example3", 0);
}
