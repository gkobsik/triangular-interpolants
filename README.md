# TriangularInterpolants

Provides different interpolants for values over triangular meshes. \
Currently supporting only the Clough-Tocher (CT) interpolant.\
For examples of usage refer to: [TriangularInterpolants_Examples](https://www.graphics.rwth-aachen.de:9000/gkobsik/TriangularInterpolants_Examples)\
\
Documentation-Version: `dev-0.2.0`

**Table of Contents**

[[_TOC_]]

## src

### TriangularInterpolant2D

doc-ver: `dev-0.2.0`

Wrapped class to allow for later easier extensions to different interpolant algorithms.

### CloughTocherInterpolant

doc-ver: `dev-0.2.0`

Provides an API to execute the interpolation and output the results. \
The implementation is following the paper: [Mann, "Cubic precision Clough-Tocher interpolation", 1998](https://cs.uwaterloo.ca/research/tr/1998/15/CS-98-15.pdf)

##### CloughTocherInterpolant

doc-ver: `dev-0.2.0`

    CloughTocherInterpolant::CloughTocherInterpolant(
        std::shared_ptr<TriMesh> mesh,
        std::vector<std::vector<float>> values,
        CTType type
        )
        
Creates an object that holds the results of the CT interpolation with the given mesh and the given values based on the desired type.
The CT interpolation is performed upon the creation.

- *mesh* - is a [TriMesh](trimesh) class, which holds informations about the topology of the mesh. Only the vertices positions and triangles need to be provided.
The normal values needed for the CT algorithm are computet for each vertex at the beginning of the algorithm. The normals are computet by the [computeNormals()](#computenormals) function.

- *values* - are values which 'live' above the vertices. These values will be interpolated. The vertex count of the mesh and the number of provided values have to match.
Furthermore the dimensionality of the vertex attribute of the mesh and the dimensionality of the provided values has to match. 
Each dimension of the value is processed indepedently of the other dimensions, e.g. performing a CT interpolation an a 2-dimensional input yields the same results as
performing the CT interpolation twice, each on a single dimension of the input and concatenating the results afterwards.

- *type* - the type of the CT interpolation that will be performed. Currently only *CTType::CT_BASIC* is supported.

One can imagine the CT interpolation of values, as the height (values) of each vertex beeing smoothly interpolated between every neighboring vertex, resulting in a smooth surface,
that can be continuously evaluated at every point within the mesh. The CT algorithm uses Bezier surfaces to describe the computed function. These surfaces are represented 
by Bezier control points which are stored by the class. One can easily evaluate a point on a surface described by Bezier control points by applying the DeCasteljau algorithm.
The resulting mesh is C1 smooth (differentiable). To obtain this smoothness, each triangle of the mesh (macro triangle) is split into 3 triangles (micro triangles) by adding an 
additional vertex in the middle of the macro triangle. Than a Bezier surface is computet over each micro triangle that guarantees differentiability over the edges of 
two neighboring triangles. For further explanation referr to the technical report of Mann, "Cubic precition Clough-Tocher interpolation", 1998, 
which describes in the second paragraph the CT splitt in 6 successive steps.

The output of the interpolation at as specific point can be queried by [getValueAt()](getvalueat) or one can get the [TriMeshCT](trimeshct) object, 
that holds the result of the CT interpolation by calling [getMesh()](getmesh).

Currently there is no update function, that allows for fast updates of few values/heights, although it is possible, since changes in the input variables affect the 
interpolated surface only locally.

WARNING: The differences in heights between two neighboring triangles should be not greater than the edge length of the triangle. e.g. the ratio of the smallest edge length and
the biggest difference to its 1-ring values should be below 1 to prevent numerical instabilities and "spikes" or "holes" in the interpolated surface.
This issue could be automatically checked, but an appropriate threshold for the ratio or another metric has to be investigated.

##### getValueAt

doc-ver: `dev-0.2.0`

    float
    CloughTocherInterpolant::getValueAt(
        glm::vec2 position,
        uint index
        )
        
Returns a floating point value (height) of the interpolated surface. 

- *position* - a two dimensional position within the mesh coordinates.

- *index* - default: 0. If the dimensionality of the values is higher than one you can specify an index to querry for 
the desired dimension of the output value, otherwise it returns the interpolated value of the first dimension at the desired point. 

Uses internally the DeCasteljau algorithm to evaluate the computet Bezier control points at the given location. For further details refer to [DeCasteljauAlgorithm](decasteljaualgorithm).

Warning: Has a very long execution time for positions that lie outside of the mesh. Returns 0 (defaul value) for values outside the input mesh.
        
##### getMesh

doc-ver: `dev-0.2.0`

    std::shared_ptr<TriMesh>
    CloughTocherInterpolant::getMesh(
        )
        
Returns a pointer to the mesh object with computed Bezier control points. It can be assumed, that the CT interpolation is already performed on the returned mesh.
The returned object type is [TriMeshCT](trimeshct). Refer to [evaluateBezierPatchAt()](evaluatebezierpatchat) for an in depth explanation of the mapping of the data structure to the DeCasteljau algorithm.

##### getHeightVector 

doc-ver: `dev-0.2.0`

    std::vector< std::vector < std::vector<float>>>
    CloughTocherInterpolant::getHeightVector(
        glm::vec2 resulution,
        glm::vec2 cropRange
        )
        
Returns a 3-dimensional vector which describes the interpolated values above a rasterized mesh with the given resolution.

- *resolution* - desribes the resolution of the output vector, e.g. describes how many points are evaluated on a uniformly sampled grid.

- *cropRange* - default: glm::vec2(FLT_MIN, FLT_MAX). Describes a optional range, to which the values are cropped, e.g. if they exceed a certain minimal or maximal threshold. 
If not delivered, no cropping is performed on the values. The lower crop threshold has to be strictly smaller than the heigher crop threshold.

The output vector is indexed in the following order: dimension, x-position, y-position. e.g. We want to access the value of a 1-dimensional input value vector at the
position (28,16). The interpolated value can by accessed by:
    
    std::vector<std::vector<std::vector<float>>> result = ctInterpolant.getHeightVector(glm::vec2(100,100));
    float value = result[0,28,16];
    
Even if the input of the CT interpolant consisted of a 1-dimensional vector, we get an output that refers to the dimensionality specified in *TriMeshCT::n_CTvalues*.

Warning: The function may become very slow for high resolutions and meshes with a low coverage of the area (bounding rect of the given mesh).

The progress of the computation is printed for every fully computed row vector to give the user an approximation of the remaining computation time.
e.g. a texture with a resolution of 4k would take approximately 30 days to compute with a low coverage of as less than 10% on a single core of an Intel(R) Core(TM) i7-4770 CPU @ 3.40GHz.

##### computeNormals

doc-ver: `dev-0.2.0`

    bool
    CloughTocherInterpolant::computeNormals(
        NormalType type
        )
        
Computes the normal vector for each vertex on the given triangular mesh. A separate normal is computed for every dimension of the input value (height) vector.

- *type* - defines the type of approximation for the normal estimation at each vertex. Currently only *NormalType::LEAST_SQUARES* is available.

A normal is computed by fitting a plane to the 1-ring of vertices in a least squares manner. 
Uses [linear_least_square_fitting_3()](https://doc.cgal.org/latest/Principal_component_analysis/group__PkgPrincipalComponentAnalysisDLLSF3.html) from the 
[CGAL](https://www.cgal.org/) library internally. Additionally the function relies on parallelisation by [OpenMP](https://www.openmp.org/).
    
##### computeControlPointsXZPositions

doc-ver: `dev-0.2.0`

    bool
    CloughTocherInterpolant::computeControlPointsXZPositions(
        )
        
Computes the xz-positions for every Bezier control point based on the local triangle vertices. 

The relative position in the xz-plane depends only on the triangle corners, as each triangle (macro triangle) is subdivided in 3 subtriangles (micro triangles). 
Every Bezier control point of the micro triangle is positioned uniformly on the edge of the triangle. Therefore it is possible to precompute these positions 
without considering the neighbourhood of the triangle. Note that the paper uses the x, y coordinates in contrast to the code base using x and z (y and z coordinates are switched).

```math
P_{i,j,k}[x,y] = \frac{iD_0 + jD_1 + kD_2}{n}
```

##### CT_step1

doc-ver: `dev-0.2.0`

    bool
    CloughTocherInterpolant::CT_step1(
        std::vector<std::vector<float>> values
        )
        
Performs the first step described in [Mann, "Cubic precision Clough-Tocher interpolation", 1998](https://cs.uwaterloo.ca/research/tr/1998/15/CS-98-15.pdf).

Assigns the provided heights (values, y-axis) to every vertex in the mesh.

```math
Set\: V_i\:  to\:  P_i
```
        
##### CT_step2_3        

doc-ver: `dev-0.2.0`

    bool
    CloughTocherInterpolant::CT_step2_3(
        )
        
Performs the second and third step described in [Mann, "Cubic precision Clough-Tocher interpolation", 1998](https://cs.uwaterloo.ca/research/tr/1998/15/CS-98-15.pdf).

Sets the height (y-axis) of every Bezier control point neighboring a triangle corner to lie in a plane which is spanned by the position and the normal of the vertex.

```math
Set \ T_{ij} \ and \ I_{i1} \ to \ lie \ in \ tangent \ plane\ at \ V_i.
```

Note that the Bezier control point $`I_1`$ is a property of the half-edge (refer to [TriMeshCT](trimeshct)). We need to check, if the halfedge is also pointing to a face. 
If it is not pointing to a face, than the half-edge is a border edge and the corresponding point $`I_1`$ does not exist. Therefore it should not be computed nor assigned.

        
##### CT_step4        

doc-ver: `dev-0.2.0`

    bool
    CloughTocherInterpolant::CT_step4(
        )
        
Performs the fourth step described in [Mann, "Cubic precision Clough-Tocher interpolation", 1998](https://cs.uwaterloo.ca/research/tr/1998/15/CS-98-15.pdf).

Compute the cross-boundary tangent vector field for every edge of two neighboring triangles and set the height of the corresponding centers to lie in the same plane.
This enforces the $`C1`$ continuity accross the triangle borders. 

```math
Set \ C_{i} \ to \ be \ coplanar \ with \ T_{jk}, \ T_{kj} \ and \ \overline{C_i}.
```

In the case that we do not have a neighboring triangle at the edge, we are at the border of the triangle mesh. We can choose an arbitary value as we do not have any constraints
at those points. To ensure a smooth and regulary looking surface, we mirror the microtriangle along the $`V_jV_k`$ boundary. Note that we introduce artificial boundary constraints, to handle edge cases that were not described in the referenced paper.

##### CT_step5

doc-ver: `dev-0.2.0`

    bool
    CloughTocherInterpolant::CT_step5(
        )

Performs the fifth step described in [Mann, "Cubic precision Clough-Tocher interpolation", 1998](https://cs.uwaterloo.ca/research/tr/1998/15/CS-98-15.pdf).

Computes the height of the second point $`I_2`$ lying on the edge between the vertex and the center of the macro triangle. This point is forced to lie in the plane spanned by the first point on the edge and the two neighboring centers of the micro triangles.

```math
Set \ I_{i2} \ to \ lie \ in \ the \ plane \ spanned \ by \ C_j, \ C_k, \ and \ I_{i1}.
```

##### CT_step6

doc-ver: `dev-0.2.0`

    bool
    CloughTocherInterpolant::CT_step6(
        )

Performs the sixth step described in [Mann, "Cubic precision Clough-Tocher interpolation", 1998](https://cs.uwaterloo.ca/research/tr/1998/15/CS-98-15.pdf).

Computes the height of the center of the macro triangle to be the average of all three neighboring points (computed in [Step 5](ct_step5) ).

````math
Set \ S \ to \ (I_{02}+I_{12}+I_{22}) / 3.
````

##### evaluateBezierPatchAt

doc-ver: `dev-0.2.0`

    float
    CloughTocherInterpolant::evaluateBezierPatchAt(
        glm::vec2 position,
        uint index
        )

Evaluates the triangle mesh surface at the given position using the DeCasteljau algorithm. Returns the height as a single floating point value. 

- *position* - desired position, at which the surface is evaluated.

- *index* - default: 0, allows to specify the dimension of the input value which has to be evaluated, if the value input was provided with more than one dimension. 

The function performs sequentially three tasks. a) It searches the mesh for the corresponding triangle containing the input position [findTriangleContainingPointXZ()](findtrianglecontainingpointxz). b) It computes the barycentric coordinates for the given position given the triangle vertices [computeBarycentricCoords()](computebarycentriccoords). c) It evaluates the Bezier control points using the DeCasteljauAlgorithm [wrapperTriMeshCT()](wrappertrimeshct) and returns the value.

Warning: the function [findTriangleContainingPointXZ()](findtrianglecontainingpointxz) has a long runtime for points outside of the mesh.

## Geometry

### TriMesh

doc-ver: `dev-0.2.0`

    class TriMesh : public OpenMesh::PolyMesh_ArrayKernelT<>
    
The TriMesh class derives all properties from [PolyMesh_ArrayKernelT<>](https://www.graphics.rwth-aachen.de/media/openmesh_static/Documentations/OpenMesh-7.1-Documentation/a02237.html). The [OpenMesh](OpenMesh/OpenMesh) library allows to represent the connections within the mesh using [halfedge data  structure](https://www.graphics.rwth-aachen.de/media/openmesh_static/Documentations/OpenMesh-7.1-Documentation/a03930.html) and thus gain a compact and efficent mesh representation. 


 ![halfedge data structure](https://www.graphics.rwth-aachen.de/media/openmesh_static/Documentations/OpenMesh-7.1-Documentation/halfedge_structure3.png) 
 
- Each vertex references one outgoing halfedge, i.e. a halfedge that starts at this vertex (1). 
- Each face references one of the halfedges bounding it (2). 
- Each halfedge provides a handle to
    - the vertex it points to (3),
    - the face it belongs to (4)
    - the next halfedge inside the face (ordered counter-clockwise) (5), 
    - the opposite halfedge (6),
    - (optionally: the previous halfedge in the face (7)).

#### readFromObj

doc-ver: `dev-0.2.0`

    void
    TriMesh::readFromObj (
        const std::string &filename
        )

Uses the [lean assimp library](ptrettner/assimp-lean) to load OBJ-files and convert them into a [TriMesh](trimesh) object.

- *filename* - a relative or absolute path to the OBJ-file.

#### readVertices

doc-ver: `dev-0.2.0`

    std::vector<OpenMesh::VertexHandle>
    TriMesh::readVertices(
        aiMesh *theMesh
        )

Iterates over aiMesh from assimp and creates a vertex in the TriMesh object for every vertex in aiMesh. Returns a vector consisting of vertex handles of TriMesh, which is sorted in the input order.

- *theMesh* - a pointer to the assimp aiMesh object.

#### readFaces

doc-ver: `dev-0.2.0`

    std::vector<OpenMesh::FaceHandle>
    TriMesh::readFaces(aiMesh *theMesh,
        std::vector<OpenMesh::VertexHandle> &vhandle
        )

Iterates over aiMesh from assimp and creates a triangle in the TriMesh object for every triangle in aiMesh. Returns a vector consisting of face handles of TriMesh, which is sorted in the input order.

- *theMesh - a pointer to the assimp aiMesh object.
- *vhandle* - vector of vertex handles. The function expects the input vector to match the vertices in the object referenced by *theMesh* in terms of order and quantity.

### TriMeshCT

doc-ver: `dev-0.2.0`

The TriMeshCT class extends the base [TriMesh](trimesh) class by Bezier control points, that are bounded to the [face](face-properties), the [vertices](vertex-properties) and the [halfedges](halfedge-properties) of the mesh.

- Every [macro triangle](macro-triangle) is represented by a face.
- Every [micro triangle](micro-triangle) is represented by a halfedge that points to a face. 

The image represents the naming convention of the Bezier control points within the code. The black triangle is the current triangle, while the gray triangle is a neighboring triangle. Note that the names of the Bezier control points are the same for both triangles and only taking care of correct referenced halfedges allows to differentiate between them.

![alt text](img/TriangleNaming.svg "Triangle with naming conventions for Bezier control points.")

#### Macro Triangle

doc-ver: `dev-0.2.0`

Every macro triangle is represented by a face. The vertices of a macro triangle can be directly accessed by iterating over the vertices of a face.

#### Micro Triangle

doc-ver: `dev-0.2.0`

Every micro triangle is represented by a halfedge that points to a face. Only half of the control points are stored directly in the halfedge. 

To iterate over all Bezier control points of a micro triangle, we need to access 

- the vector the halfedge is outgoing from,
- the face the halfeedge is pointing to,
- the opposite halfedge,
- the next halfedge.

The image representd which Bezier point can be accessed by which halfedge, e.g. which halfedge holds the information for the point. The coloring represents the belonging to a halfedge. Halfarrows represent halfedges. Dotted arrows represent which halfedges or facas can be accessed from the current halfedge. The current halfedge is blue.

![alt text](img/TriangleLinks.svg "Triangle with correspondences of Bezier control points to halfedges.")

#### Control Point

doc-ver: `dev-0.2.0`

    struct ControlPoint {
        glm::vec2 pos;
        std::array<float, NUM_OF_VALUES> val;
    };

A structure that defines a Bezier control point. The position in the xz-plane of a Bezier point is constant in the CT interpolation for all height values over the given mesh. Therefore we reduce the memory print by merging one xz-position value with a vector of height values.
The number of height values has to be known at compile time and therefore we have to define them a priori in *NUM_OF_VALUES*.
    
    #define NUM_OF_VALUES 2
    const static uint n_CTvalues = NUM_OF_VALUES;
    
*NUM_OF_VALUES* defines the dimension of the height vector, e.g. how many different height values are required and processed per Bezier control point. Note that this define also sets the required dimensionality of the input values when creating a new [TriMeshCT](trimeshct) object.
    
#### Normals    

doc-ver: `dev-0.2.0`

    struct Normals {
        std::array<glm::vec3, NUM_OF_VALUES> normal;
    };
    
A structure that defines a normal vector for every vertex. 

Although a Bezier control point is highly correlated with the normal a separate data structure allows to decouple the normal of a vertex from its position. This allows to access the position and the normal in a similar way. 
Note that the number of vectors that are stored in the struct depends on the number of height values per control point, e.g. the dimensionality of the height vector is the same as the dimensionality of the normal vector.
    
    
#### Vertex Properties

doc-ver: `dev-0.2.0`

    OpenMesh::VPropHandleT<TriMeshCT::ControlPoint> TriMeshCT::V =
        OpenMesh::VPropHandleT<TriMeshCT::ControlPoint>();

    OpenMesh::VPropHandleT<TriMeshCT::Normals> TriMeshCT::N =
        OpenMesh::VPropHandleT<TriMeshCT::Normals>();

A vertex holds the *V* Bezier control point and *N* normals at the vertex position in the mesh structure.

- *V* - vector position and height values, Bezier control point $`V`$
- *N* - corresponding normals at the vector position

The vertex positions are stored twice in the mesh structure, once in the *pos* propertie and second in the *V* propertie of a vertex. The second formulation is partially redundand, but allows to decouple a Bezier control point from the mesh vertex. A mesh vertex pos is a 3-dimensional vector, where the V propertie of the vertex defines an higher dimensional vector, with constant positions in the xz-plane and multiple height values (y-plane).

#### Face Properties

doc-ver: `dev-0.2.0`

    OpenMesh::FPropHandleT<TriMeshCT::ControlPoint> TriMeshCT::S =
        OpenMesh::FPropHandleT<TriMeshCT::ControlPoint>();
        
A face holds the *S* Bezier control point in the mesh structure.

- *S* - defines the center of the macro triangle
 
The Bezier control point *S* is shared between every micro triangle and thus can be efficiently stored as the property of the face. We can access it easily as the outgoing halfedge of a vertex is pointing to the face.

#### Halfedge Properties

doc-ver: `dev-0.2.0`

    OpenMesh::HPropHandleT<TriMeshCT::ControlPoint> TriMeshCT::T =
        OpenMesh::HPropHandleT<TriMeshCT::ControlPoint>();

    OpenMesh::HPropHandleT<TriMeshCT::ControlPoint> TriMeshCT::C =
        OpenMesh::HPropHandleT<TriMeshCT::ControlPoint>();

    OpenMesh::HPropHandleT<TriMeshCT::ControlPoint> TriMeshCT::I1 =
        OpenMesh::HPropHandleT<TriMeshCT::ControlPoint>();

    OpenMesh::HPropHandleT<TriMeshCT::ControlPoint> TriMeshCT::I2 =
        OpenMesh::HPropHandleT<TriMeshCT::ControlPoint>();

A halfedge holds the *T*, *C*, *I1* and *I2* Bezier control points.

- *T* - the first Bezier control point in the direction of the vector on the outer edge of the macro triangle
- *C* - the center of the micro triangle 
- *I1* - the first Bezier control point in the direction of the center of the micro triangle
- *I2* - the second Bezier control point in the direction of the center of the micro triangle

The Bezier control point *C* is used exclusively for the micro triangle corresponding to the halfedge.
The Bezier control points *T*, *I1* and *I2* are shared between two neighboring micro triangles. 

#### getPosValue

doc-ver: `dev-0.2.0`

    glm::vec3
    TriMeshCT::getPosValue(
        TriMeshCT::ControlPoint cp,
        uint valueIdx
        )

Returns a 3-dimensional vector with the position of the Bezier control point in the xz-plane and the height in the y-plane.

- *cp* - [Bezier control point](control-point) in the [CT mesh structure](trimeshct).
- *valueIdx* - Index the the dimension of the height vector.
 
Note that the height value dimensionality of the corresponding [Bezier control point](control-point) might be higher than one and thus we need to specify which value we want to receive in the 3-dimensional output value. The output is ordered as follows *(x-position, height[valudIdx], z-position)*.

#### getValue

doc-ver: `dev-0.2.0`

    float
    TriMeshCT::getValue(
        TriMeshCT::ControlPoint cp,
        uint valueIdx
        )

Returns the height value of the corresponding [Bezier control point](control-point).

- *cp* - [Bezier control point](control-point) in the [CT mesh structure](trimeshct).
- *valueIdx* - Index the the dimension of the height vector.

### TriMeshUtils

#### getTriangleHalfedgeHandles

doc-ver: `dev-0.2.0`

    static std::vector<OpenMesh::HalfedgeHandle>
    getTriangleHalfedgeHandles (
            std::shared_ptr<TriMesh> mesh,
            OpenMesh::FaceHandle triangle
            )

Returns three halfedges pointing in counter-clockwise order.

- *mesh* - a pointer to a [TriMesh](trimesh) object.
- *triangle* - a handle to the triangle.

Iterates over a triangle in counter-clockwise order and returns always three halfedges. The order of the output is deterministic, as the pointer from the face to a halfedge is static as long we do not modify the mesh.

#### getTriangleCornerPositionsXZ

doc-ver: `dev-0.2.0`

    static std::vector<glm::vec2>
    getTriangleCornerPositionsXZ (
            std::shared_ptr<TriMesh> mesh,
            OpenMesh::FaceHandle triangle
            )
            
Returns three 2-dimensional vectors with the position of the triangle vertices in the xz-plane.

- *mesh* - a pointer to a [TriMesh](trimesh) object.
- *triangle* - a handle to the triangle.

Iterates over a triangle in counter-clockwise order and returns always three vectors. The order of the output is deterministic, as the pointer from the face to a halfedge is static as long we do not modify the mesh.

#### getTriangleCornerPositionsXYZ

doc-ver: `dev-0.2.0`

    static std::vector<glm::vec3>
    getTriangleCornerPositionsXYZ (
            std::shared_ptr<TriMesh> mesh,
            OpenMesh::FaceHandle triangle
            )

Returns three 3-dimensional vectors with the position of the triangle vertices in the xyz-plane.

- *mesh* - a pointer to a [TriMesh](trimesh) object.
- *triangle* - a handle to the triangle.

Iterates over a triangle in counter-clockwise order and returns always three vectors. The order of the output is deterministic, as the pointer from the face to a halfedge is static as long we do not modify the mesh.

#### findTriangleContainingPointXZ

doc-ver: `dev-0.2.0`

    static bool
    findTriangleContainingPointXZ (
            std::shared_ptr<TriMesh> mesh,
            glm::vec2 point,
            unsigned int maxSearchIterations,
            OpenMesh::FaceHandle startSearchTriangle,
            OpenMesh::FaceHandle &return_result
            )
            
Computes whether the *point* in the plane lies within a triangle or not. Returns the corresponding triangle in *return_result* if the search was successful.

- *mesh* - a pointer to a [TriMesh](trimesh) object.
- *point* - a 2-dimensional point in the xz-plane.
- *maxSearchIterations* - maximum number of iterations before an early out.
- *startSearchTriangle* - defines a start point for the algorithm.
- *return_result* - possible a handle to a triangle containing *point*. Is unchanged if the result is *false*, check for *null_ptr*.

Iterates over all faces in the mesh, until it finds a triangle containing the *point*, all faces in the mesh are checked or the maximum number of search iterations is reached. Checks *startSearchTriangle* first to allow for an early out, if we expect the point to lie within a specific triangle, e.g. the last search result.
Note, that the search iterates over all triangles, if the point lies outside of the *mesh* and is therefore very slow points outside the mesh.


#### createSimplePlaneCT

doc-ver: `dev-0.2.0`

    static std::shared_ptr<TriMesh>
    createSimplePlaneCT(
            uint width,
            uint height,
            float scale
            )

Creates a flat rectangular mesh consisting of triangular patches in a regular grid. The size of the grid can be specified in the parameters.

- *width* - number of rectangular patches in the x-direction
- *height* - number of rectangular patches in the z-direction
- *scale* - default: 1.0, defines a scaling factor for the size of the mesh.

The parameters *width* and *height* define the size and thus the number of rectangular patches created. Each patch ist 3 units wide in x- and z-direction with a default scaling factor of 1.0 and consists of two triangles. 

This function is used to create artificial inputs and test the functionality of the CT interpolant.

## Utils

### DeCstaljauAlgorithm

#### wrapperTriMeshCT

doc-ver: `dev-0.2.0`

    static glm::vec3
    wrapperTriMeshCT(
            std::shared_ptr<TriMesh> mesh,
            TriMesh::HalfedgeHandle he_handle,
            glm::vec2 uv,
            uint degree,
            uint index
            )

Returns the height and position of the evaluated Bezier patch of the given *mesh* of the triangle indexed by *he_handle* at position defined by the barycentric coordinates *uv*.

- *mesh* - a pointer to a [TriMesh](trimesh) object.
- *he_handle* - a pointer to the desired triangle
- *uv* - barycentric coordinates of the desired point
- *degree* - degree of the Bezier patch. Currently only 3 is supported.
- *index* - defines which dimension of the height input value has to be evaluated, is the value input was provided with more than one dimension.

Evaluates a triangle indexed by *he_handle* of the given *mesh*. Transforms the Bezier control points of the specified triangle from the *mesh* into a specialized data structure created by [triangleBezierIndizies](triangleBezierIndizies) which is indexed by the Bezier ordinates. Afterwards if passes the data structure to [evaluateTriangle](evaluateTriangle) which performs the deCasteljau Algorithm and returns the desired height value.

#### evaluateTriangle
    
doc-ver: `dev-0.2.0`    
    
    static glm::vec3
    evaluateTriangle(
            std::unordered_map<glm::vec<3, uint>, glm::vec3> controlPoints,
            uint degree,
            glm::vec2 uv
            )
            
Executes the deCasteljau algorithm on the provided data and returns the vector containing position and height. See the referenced document for in depth explanation of the algorithm.            

- *controlPoints* - awaits the Bezier control points in a specialized structure, which indexes the points by Bezier ordinates as the key and holds the Bezier control points as the value.
- *degree* - defines the degree of the Bezier patch. Currently only degree of 3 is supported. Other values would lead to errors and cannot by handled by *controlPoints*.
- *uv* - the barycentric coordinates of the point we want to evaluate

Docu: Hansford, "Handbook of Computer Aided Geometrical Design", 2002
Chapter 4.4.3 The de Casteljau algorithm for Bezier triangles

#### triangleBezierIndizies

doc-ver: `dev-0.2.0`

    static std::unordered_map<glm::vec<3, uint>, glm::vec3>
    triangleBezierIndizies(
            int degree
            )

Creates an unordered map that maps a 3-dimensional vector, representing Bezier coefficients, to an other 3-dimensional vector, representing Bezier control points. 

- *degree* - degree of the Bezier triangle.

The first vector of the map consists of every combination of possible integer values that sum up to the *degree* value, representing the Bezier coefficients of a certain *degree*. The second vector is initially empty (0 values), but can be filled to hold Bezier control points.

### GeometryFunc

#### computeLeastSquaresPlaneNormal

doc-ver: `dev-0.2.0`

    glm::vec3
    computeLeastSquaresPlaneNormal(
            const std::vector<glm::vec3> &points
            )

Fits a plane using the least squares algorithm to fit the given points.

- *points* - a vector consisting of points in 3D space.

Uses [linear_least_squares_fitting_3](https://doc.cgal.org/latest/Principal_component_analysis/group__PkgPrincipalComponentAnalysisDLLSF3.html) from [CGAL](https://www.cgal.org/).
            
#### computePlanarBarycentricCoordinates

doc-ver: `dev-0.2.0`            
            
    glm::vec3
    computePlanarBarycentricCoordinates(
            std::vector<glm::vec2> triangleVertices,
            glm::vec2 querryPoint
            )

Computes the barycentric coordinates of a two dimensional triangle. Returns a three dimensional vector holding the u,v,w - coordinates.

- *triangleVertices* - vertices of a two dimensional triangle. Vector has to consist of exactly 3 items.
- *querryPoint* - a point in space. It may lie inside or outside of the triangle

Uses [Triangle_coordinates_2](https://doc.cgal.org/latest/Barycentric_coordinates_2/index.html) from [CGAL](https://www.cgal.org/).
            
#### computeHeightPointNormal

doc-ver: `dev-0.2.0`            
            
    float
    computeHeightPointNormal(
            glm::vec3 refPoint,
            glm::vec3 normal,
            glm::vec3 point
            )

Computes the height (y coordinate) of the given *point* on a plane defined by *refPoint* and a plane *normal*. Returns the computed height.

- *refPoint* - reference point lying on the defines plane.
- *normal* - normal vector of the plane. Note that the input should be normalized.
- *point* - a point in space. Only the x and z values are used.

The formula evaluates the y value of a plane defined by a point and normal given x- and z-value of *point*.

#### computeHeight3Point

doc-ver: `dev-0.2.0`

    float
    computeHeight3Point(
            glm::vec3 refPoint1,
            glm::vec3 refPoint2,
            glm::vec3 refPoint3,
            glm::vec3 point
            )

Computes the height (y coordinate) of the given *point* on a plane defined by three points. Returns the computet height.

- *refPoint1* - first corner of a reference triangle.
- *refPoint1* - second corner of a reference triangle.
- *refPoint1* - third corner of a reference triangle.
- *point* - a point in space. Only the x and z values are used.

The formula evaluates the y value of a plane defined by *refPoint1*, *refPoint2* and *refPoint3* given x- and z-value of *point*.

#### reflectPointOnLine2D

doc-ver: `dev-0.2.0`

    glm::vec2
    reflectPointOnLine2D(
            glm::vec2 point,
            glm::vec2 lineVector
            )
    
Mirrors a point on given a line. Returns a two dimensional vector which is the mirrored point.

- *point* - two dimensional point.
- *lineVector* - a two dimensional vector, which represents a direction that spans a line in space.

#### computeBarycentricCoords

doc-ver: `dev-0.2.0`

    glm::vec2
    computeBarycentricCoords(
            glm::vec2 x,
            glm::vec2 i,
            glm::vec2 j,
            glm::vec2 k
            )

Warning: redundand definition of functionality. Should delete the slower implementation.            
Computes the barycentric coordinates of a point and a triangle defined by three points. Returns a two dimensional vector which holds the u- and v-coordinates.

- *x* - two dimensional point of which we want to compute the barycentric coordinates.
- *i* - represents one corner af a two dimensional triangle.
- *j* - represents one corner af a two dimensional triangle.
- *k* - represents one corner af a two dimensional triangle.

The third coordinate of the Barycentric coordinates can be easily obtained by computing 1 - u - v.
The method uses the proposed Cramer's rule to solve a linear system with lesser multiplications. Referr to [forum post](https://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates) for further explanations.

#### getBarycentricCoords

doc-ver: `dev-0.2.0`

    glm::dvec3
    getBarycentricCoords(
            glm::dvec2 pt,
            glm::dvec2 v1,
            glm::dvec2 v2,
            glm::dvec2 v3
            )
            
Warning: redundand definition of functionality. Should delete the slower implementation.
Computes the barycentric coordinates of a point and a triangle defined by three points. Returns a two dimensional vector which holds the u- and v-coordinates.

- *x* - two dimensional point of which we want to compute the barycentric coordinates.
- *i* - represents one corner af a two dimensional triangle.
- *j* - represents one corner af a two dimensional triangle.
- *k* - represents one corner af a two dimensional triangle.

#### isPointInTriangle2D

doc-ver: `dev-0.2.0`            

    bool
    isPointInTriangle2D(
            glm::vec2 pt,
            glm::vec2 v1,
            glm::vec2 v2,
            glm::vec2 v3
            )

Checks whether a given point is inside a triangle.

- *pt* - two dimensional point, we want to check.
- *v1* - represents one corner af a two dimensional triangle.
- *v2* - represents one corner af a two dimensional triangle.
- *v3* - represents one corner af a two dimensional triangle.
 
The function checks, whether all elements of the Barycentric coordinates lie in the range of 0.0 (inclusive) and 1.0 (inclusive) to determine if the point lies inside or outside of a triangle.