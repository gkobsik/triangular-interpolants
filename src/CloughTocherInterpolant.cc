#include "CloughTocherInterpolant.hh"

#include "utils/GeometryFunc.hh"
#include "utils/DeCasteljauAlgorithm.hh"

#include "geometry/TriMeshUtils.hh"

#include <iostream>
#include <fstream>
#include <cassert>

/// ##############
/// ### PUBLIC ###
/// ##############

CloughTocherInterpolant::CloughTocherInterpolant(
    std::shared_ptr<TriMeshCT> mesh,
    std::vector<std::vector<float>> values,
    CTType type
)
{
    assert(mesh != nullptr);
    assert(std::size(mesh->vertices()) == values.size());
    assert(TriMeshCT::n_CTvalues == values[0].size());

    this->mesh = mesh;

    if (!computeControlPointXZPositions()) //parallel
        std::cout << "WARNING: Compute Control Points failed!" << std::endl;

    switch (type)
    {
    case CTType::CT_BASIC:
        if (!CT_step1(values))
            std::cout << "WARNING: Step 1 of Clough Tocher Interpolation failed!" << std::endl;        
        if (!computeNormals()) //parallel
            std::cout << "WARNING: Compute Normals failed" << std::endl;
        if (!CT_step2_3())
            std::cout << "WARNING: Step 2 or 3 of Clough Tocher Interpolation failed!" << std::endl;
        if (!CT_step4())
            std::cout << "WARNING: Step 4 of Clough Tocher Interpolation failed!" << std::endl;
        if (!CT_step5())
            std::cout << "WARNING: Step 5 of Clough Tocher Interpolation failed!" << std::endl;
        if (!CT_step6())
            std::cout << "WARNING: Step 6 of Clough Tocher Interpolation failed!" << std::endl;        
        break;
    case CTType::MODIFIED_FARIN:
        std::cout << "Not implemented, yet." << std::endl;
        break;
    case CTType::CUBIC:
        std::cout << "Not implemented, yet." << std::endl;
        break;
    case CTType::QUARTIC:
        std::cout << "Not implemented, yet." << std::endl;
        break;
    }
}

CloughTocherInterpolant::CloughTocherInterpolant(
    std::shared_ptr<TriMeshCT> mesh
)
{
    assert(mesh != nullptr);
    this->mesh = mesh;
}

float
CloughTocherInterpolant::getValueAt(
    tg::pos2 position,
    int index
)
{
    assert(index < TriMeshCT::n_CTvalues);
    return evaluateBezierPatchAt(position, index);
}

std::shared_ptr<TriMeshCT>
CloughTocherInterpolant::getMesh(
)
{
    return mesh;
}

/// ###############
/// ### PRIVATE ###
/// ###############

bool
CloughTocherInterpolant::computeNormals(
    NormalType type
)
{
    bool result = true;
    mesh->compactify();

    switch (type)
    {
    case NormalType::LEAST_SQUARES:
        #pragma omp parallel for
        for (int offset = 0; offset < mesh->vertices().size(); ++offset)
        {
            auto vh = mesh->vertices()[offset];
            auto vertex = mesh->V[vh];

            for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
            {
                std::vector<tg::pos3> oneRingVertices;
                oneRingVertices.push_back(tg::pos3(vertex.pos[0], vertex.val[i], vertex.pos[1]));

                for (auto vh_n : vh.adjacent_vertices())
                {
                    auto vertex_n = mesh->V[vh_n];
                    oneRingVertices.push_back(tg::pos3(vertex_n.pos[0], vertex_n.val[i], vertex_n.pos[1]));
                }

                mesh->N[vh].normal[i] = GeometryFunc::computeLeastSquaresPlaneNormal(oneRingVertices);
            }          
        }
        return result;
    }

    return false;
}

bool
CloughTocherInterpolant::computeControlPointXZPositions(
)
{
    bool result = true;
    mesh->compactify();

    #pragma omp parallel for
    for (int offset = 0; offset < mesh->vertices().size(); ++offset)
    {
        auto vh = mesh->vertices()[offset];
        auto pos = mesh->position[vh];
        mesh->V[vh].pos = tg::pos2(pos[0], pos[2]);
    }

    #pragma omp parallel for
    for (int offset = 0; offset < mesh->faces().size(); ++offset)
    {
        auto fh = mesh->faces()[offset];

        auto centroid = pm::face_centroid(fh, mesh->position);
        mesh->S[fh].pos = tg::pos2(centroid.x, centroid.z);
    }

    #pragma omp parallel for
    for (int offset = 0; offset < mesh->halfedges().size(); ++offset)
    {
        auto hh = mesh->halfedges()[offset];
        auto fh = hh.face();
        auto V = mesh->V[hh.vertex_from()];

        auto outerVector = (mesh->V[hh.vertex_to()].pos - V.pos) / 3.0f;
        mesh->T[hh].pos = V.pos + outerVector;

        if (fh.is_valid())
        {
            // edge-case: border - > compute C, I1 and I2 only for HEs pointing to a valid face
            mesh->C[hh].pos = (mesh->S[fh].pos + V.pos + mesh->V[hh.vertex_to()].pos) / 3.0f;

            auto innerVector = (mesh->S[fh].pos - V.pos) / 3.0f;
            mesh->I1[hh].pos = V.pos + innerVector;
            mesh->I2[hh].pos = V.pos + innerVector * 2.0f;
        }
    }

    return result;
}

bool
CloughTocherInterpolant::CT_step1(
    std::vector<std::vector<float>> values
)
{
    bool result = true;
    mesh->compactify();

    #pragma omp parallel for
    for (int offset = 0; offset < mesh->vertices().size(); ++offset)
    {
        assert(values[offset].size() <= TriMeshCT::n_CTvalues);
        auto vh = mesh->vertices()[offset];

        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
            mesh->V[vh].val[i] = values[offset][i];

    }

    return result;
}

bool
CloughTocherInterpolant::CT_step2_3(
)
{
    bool result = true;
    mesh->compactify();

    // aggregate step 2. and 3. to avoid superfluous/repetitive code
    #pragma omp parallel for
    for (int offset = 0; offset < mesh->halfedges().size(); ++offset)
    {
        auto hh = mesh->halfedges()[offset];
        auto vh = hh.vertex_from();
        auto fh = hh.face();
        auto V = mesh->V[vh];
        auto N = mesh->N[vh];
        auto T = mesh->T[hh];
        auto I1 = mesh->I1[hh];

        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
        {
            auto pos = tg::pos3(V.pos[0], V.val[i], V.pos[1]);
            auto normal = N.normal[i];
            // 2. Set T to lie in tangent plane at V
            mesh->T[hh].val[i] = GeometryFunc::computeHeightPointNormal(pos, normal, tg::pos3(T.pos[0], 0, T.pos[1]));
            // 3. Set I to lie in tangen plane at V
            if (fh.is_valid())
            {
                // edge-case: border
                mesh->I1[hh].val[i] = GeometryFunc::computeHeightPointNormal(pos, normal, tg::pos3(I1.pos[0], 0, I1.pos[1]));
            }
        }
    }
    return result;
}

bool
CloughTocherInterpolant::CT_step4(
)
{
    bool result = true;
    mesh->compactify();

    //#pragma omp parallel for
    for (int offset = 0; offset < mesh->halfedges().size(); ++offset)
    {
        auto hh = mesh->halfedges()[offset];
        auto hh_next = hh.next();
        auto hh_o = hh.opposite();
        auto hh_o_next = hh_o.next();
        auto fh = hh.face();
        auto fh_o = hh_o.face();
 
        auto C = mesh->C[hh];
        auto T = mesh->T[hh]; 
        auto I1_rhs = mesh->I1[hh_o_next]; 
        auto I1_lhs = mesh->I1[hh];
        auto T_o = mesh->T[hh_o];
        auto I1_o_rhs = mesh->I1[hh_next];
        auto I1_o_lhs = mesh->I1[hh_o];

        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
        {
            if (fh.is_valid())
            {
                auto point1 = tg::pos3(T.pos[0], T.val[i], T.pos[1]);
                auto point2 = tg::pos3(T_o.pos[0], T_o.val[i], T_o.pos[1]);
                tg::vec3 vector_tangent;

                if (fh_o.is_valid())
                {
                    vector_tangent = tg::vec3(
                        I1_lhs.pos[0] - I1_rhs.pos[0] + I1_o_rhs.pos[0] - I1_o_lhs.pos[0],
                        I1_lhs.val[i] - I1_rhs.val[i] + I1_o_rhs.val[i] - I1_o_lhs.val[i],
                        I1_lhs.pos[1] - I1_rhs.pos[1] + I1_o_rhs.pos[1] - I1_o_lhs.pos[1]);
                }
                else
                {
                    //edge case: border, no opposing triangle on the border available
                    //compute artificial cross-boundary tangent vector field
                    //mirror the xz-position of I points on the border and calculate the height of those points

                    auto V = mesh->V[hh.vertex_from()];
                    auto V_o = mesh->V[hh_o.vertex_from()];
                    auto N = mesh->N[hh.vertex_from()];
                    auto N_o = mesh->N[hh_o.vertex_from()];

                    auto artificial_I1_o_lhs_pos = GeometryFunc::reflectPointOnLine2D(I1_lhs.pos, T.pos - T_o.pos);
                    auto artificial_I1_rhs_pos = GeometryFunc::reflectPointOnLine2D(I1_o_rhs.pos, T.pos - T_o.pos);

                    auto artificial_I1_o_lhs_val = GeometryFunc::computeHeightPointNormal(
                        tg::pos3(V_o.pos[0], V_o.val[i], V_o.pos[1]),
                        N_o.normal[i],
                        tg::pos3(artificial_I1_o_lhs_pos[0], 0, artificial_I1_o_lhs_pos[1]));
                    auto artificial_I1_rhs_val = GeometryFunc::computeHeightPointNormal(
                        tg::pos3(V.pos[0], V.val[i], V.pos[1]),
                        N.normal[i],
                        tg::pos3(artificial_I1_rhs_pos[0], 0, artificial_I1_rhs_pos[1]));

                    vector_tangent = tg::vec3(
                        I1_lhs.pos[0] - artificial_I1_rhs_pos[0] + I1_o_rhs.pos[0] - artificial_I1_o_lhs_pos[0],
                        I1_lhs.val[i] - artificial_I1_rhs_val    + I1_o_rhs.val[i] - artificial_I1_o_lhs_val,
                        I1_lhs.pos[1] - artificial_I1_rhs_pos[1] + I1_o_rhs.pos[1] - artificial_I1_o_lhs_pos[1]);

                }
                //^ define cross boundary tangent vector field
                auto vector_edge = point1 - point2;
                auto vector_normal = tg::normalize(tg::cross(vector_tangent, vector_edge));
                assert(tg::length(vector_tangent) > 0);
                assert(tg::length(vector_edge) > 0);
                //assert(tg::length(vector_normal) > 0); TODO: check for correct cross vector
                assert(vector_tangent != vector_edge);
                assert(vector_normal != vector_edge);
                mesh->C[hh].val[i] = GeometryFunc::computeHeightPointNormal(point1, vector_normal, tg::pos3(C.pos[0], 0, C.pos[1]));
            }
        }
    }
    return result;
}

bool
CloughTocherInterpolant::CT_step5(
)
{
    bool result = true;
    mesh->compactify();

    #pragma omp parallel for
    for (int offset = 0; offset < mesh->halfedges().size(); ++offset)
    {
        auto hh = mesh->halfedges()[offset];
        auto hh_prev = hh.prev();
        auto fh = hh.face();

        if (fh.is_valid()) {

            auto I1 = mesh->I1[hh];
            auto I2 = mesh->I2[hh];
            auto C1 = mesh->C[hh];
            auto C2 = mesh->C[hh_prev];

            for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
            {
                mesh->I2[hh].val[i] = GeometryFunc::computeHeight3Point(
                    tg::pos3(I1.pos[0], I1.val[i], I1.pos[1]),
                    tg::pos3(C1.pos[0], C1.val[i], C1.pos[1]),
                    tg::pos3(C2.pos[0], C2.val[i], C2.pos[1]),
                    tg::pos3(I2.pos[0], 0, I2.pos[1]));
            }
        }
    }
    return result;
}

bool
CloughTocherInterpolant::CT_step6(
)
{
    bool result = true;
    mesh->compactify();

    #pragma omp parallel for
    for (int offset = 0; offset < mesh->faces().size(); ++offset)
    {
        auto fh = mesh->faces()[offset];
        auto hh = fh.any_halfedge();

        auto I2 = mesh->I2[hh];
        auto I2_next = mesh->I2[hh.next()];
        auto I2_prev = mesh->I2[hh.prev()];

        for (int i = 0; i < TriMeshCT::n_CTvalues; ++i)
        {
            mesh->S[fh].val[i] = (I2.val[i] + I2_next.val[i] + I2_prev.val[i]) / 3.0f;
        }
    }

    return result;
}

float
CloughTocherInterpolant::evaluateBezierPatchAt(
    tg::pos2 position,
    int index
)
{
    const int degree = 3;

    // search for triangle containing 'position'
    for (auto const fh : mesh->faces())
    {
        auto const Vs = fh.vertices().to_array<3>(mesh->V);        
        auto tri = tg::triangle(Vs[0].pos, Vs[1].pos, Vs[2].pos);
        
        if (tg::contains(tri, position))
        {            
            // check which micro triangle contains 'position' (corresponging halfedge)
            for (auto hh : fh.halfedges())
            {
                auto midpoint = tg::centroid(tri);
                auto pos_from = mesh->V[hh.vertex_from()].pos;
                auto pos_to = mesh->V[hh.vertex_to()].pos;
                auto microTriangle = tg::triangle(midpoint, pos_from, pos_to);

                if (tg::contains(microTriangle, position))
                {                    
                    auto bary = GeometryFunc::computeBarycentricCoords(midpoint, pos_from, pos_to, position);
                    return DeCasteljauAlgorithm::wrapperTriMeshCT(mesh, hh, bary, degree, index).y;
                }
            }            
        }
    }    

    return 0;
}