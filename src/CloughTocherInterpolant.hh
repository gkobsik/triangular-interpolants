#pragma once

#include "TriangularInterpolant2D.hh"
#include "geometry/TriMeshCT.hh"

#include <memory>

//This class represents the Clough-Tocher splitt scheme for triangular patches.
class CloughTocherInterpolant : TriangularInterpolant2D {

    enum CTType { CT_BASIC, MODIFIED_FARIN, CUBIC, QUARTIC };
    enum NormalType { LEAST_SQUARES };

public:
    CloughTocherInterpolant(
        std::shared_ptr<TriMeshCT> mesh,
        std::vector<std::vector<float>> values,
        CTType type = CTType::CT_BASIC
    );
    // ^uses Clough-Tocher splitt scheme to compute values for controll Bezier-Patches
    // mesh - use TriMeshFactory
    // values - [i][n]: i - vertex index,  n - value over indexed vertex

    CloughTocherInterpolant(
        std::shared_ptr<TriMeshCT> mesh
    );

    // return: interpolated value at desired position
    float
        getValueAt(
            tg::pos2 position,
            int index = 0
        );

    // return: current mesh object
    std::shared_ptr<TriMeshCT>
        getMesh(
        );

    // computes normals based on 1-ring vertex neighbourhood
    bool
        computeNormals(
            NormalType type = NormalType::LEAST_SQUARES
        );

    // computes the X and Y positions of Bezier control points
    bool
        computeControlPointXZPositions(
        );    

    // Step 1 - Docu: Mann, "Cubic precision Clough-Tocher interpolation", 1998 - 2 Clough-Tocher for documentation
    bool
        CT_step1(
            std::vector<std::vector<float>> values
        );

    // Step 2 and 3 - Docu: Mann, "Cubic precision Clough-Tocher interpolation", 1998 - 2 Clough-Tocher for documentation
    bool
        CT_step2_3(
        );

    // Step 4 - Docu: Mann, "Cubic precision Clough-Tocher interpolation", 1998 - 2 Clough-Tocher for documentation
    bool
        CT_step4(
        );

    // Step 5 - Docu: Mann, "Cubic precision Clough-Tocher interpolation", 1998 - 2 Clough-Tocher for documentation
    bool
        CT_step5(
        );

    // Step 6 - Docu: Mann, "Cubic precision Clough-Tocher interpolation", 1998 - 2 Clough-Tocher for documentation
    bool
        CT_step6(
        );

    // evaluates the Bezier patch at the given position (XZ) - return: interpolated height value (Y)
    float
        evaluateBezierPatchAt(
            tg::pos2 position,
            int index = 0
        );

private://Member variables
    std::shared_ptr<TriMeshCT> mesh;

};