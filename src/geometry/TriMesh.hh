#pragma once
//#define _USE_MATH_DEFINES

#include <polymesh/Mesh.hh>
#include <polymesh/properties.hh>
#include <typed-geometry/tg.hh>

class TriMesh : public pm::Mesh 
{
public:
    pm::vertex_attribute<tg::pos3> position{ *this };
};


