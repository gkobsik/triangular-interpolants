#pragma once

#include "TriMesh.hh"

// Number of values that will be interpolated
#define NUM_OF_VALUES 2

class TriMeshCT : public TriMesh {

public:

    struct ControlPoint {
        tg::pos2 pos;
        std::array<float, NUM_OF_VALUES> val;
    };

    struct NormalVector {
        std::array<tg::vec3, NUM_OF_VALUES> normal;
    };

    const static int n_CTvalues = NUM_OF_VALUES;

    pm::vertex_attribute<ControlPoint>      V { *this };
    pm::vertex_attribute<NormalVector>      N { *this };
    pm::face_attribute<ControlPoint>        S { *this };
    pm::halfedge_attribute<ControlPoint>    T { *this }, 
                                            C { *this }, 
                                            I1{ *this }, 
                                            I2{ *this };
    //^ control points on the left hand side of the half-edge; name convention: Mann, 1998

};
