#include "TriMeshUtils.hh"

std::shared_ptr<TriMeshCT>
TriMeshUtils::createSimplePlaneCT(
    int width,
    int height,
    float scale)
{
    auto triMesh = std::make_shared<TriMeshCT>();

    std::vector<std::vector<pm::vertex_handle>> vhandle(width + 1, std::vector<pm::vertex_handle>(height + 1));

    //Add vertices:
    for (int y = 0; y <= height; ++y) {
        for (int x = 0; x <= width; ++x) {
            vhandle[x][y] = triMesh->vertices().add();
            triMesh->position[vhandle[x][y]] = tg::pos3(scale * 3 * x, scale * 0, scale * 3 * y);
        }
    }

    //Add faces:
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            //upper triangle
            triMesh->faces().add(vhandle[x][y], vhandle[x][y + 1], vhandle[x + 1][y]);            
            //lower triangle
            triMesh->faces().add(vhandle[x + 1][y], vhandle[x][y + 1], vhandle[x + 1][y + 1]);            
        }
    }

    return triMesh;
}


