#pragma once

#include <memory>

#include "TriMeshCT.hh"

class TriMeshUtils {
public:

    static std::shared_ptr<TriMeshCT>
        createSimplePlaneCT(
            int width,
            int height,
            float scale = 1.0f
        );

};