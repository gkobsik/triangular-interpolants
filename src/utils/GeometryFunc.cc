#include "GeometryFunc.hh"

/// ##############
/// ### PUBLIC ###
/// ##############

tg::vec3
GeometryFunc::computeLeastSquaresPlaneNormal(
    const std::vector<tg::pos3>& points
)
{
    assert(points.size() > 2);
    std::vector<Point> cgalPoints;
    Plane plane;
    for (auto it = points.begin(); it != points.end(); ++it) {
        cgalPoints.push_back(Point((*it).x, (*it).y, (*it).z));
    }
    linear_least_squares_fitting_3(cgalPoints.begin(), cgalPoints.end(), plane, CGAL::Dimension_tag<0>());
    auto normal = tg::vec3(
        plane.orthogonal_direction().dx(),
        plane.orthogonal_direction().dy(),
        plane.orthogonal_direction().dz());
    assert(tg::length(normal) > 0);
    return tg::normalize(normal);
}

tg::vec3
GeometryFunc::computePlanarBarycentricCoordinates(
    std::vector<tg::pos2> triangleVertices,
    tg::pos2 querryPoint
)
{
    assert(triangleVertices.size() == 3);
    const Point2  first_vertex(triangleVertices[0][0], triangleVertices[0][1]);
    const Point2 second_vertex(triangleVertices[1][0], triangleVertices[1][1]);
    const Point2  third_vertex(triangleVertices[2][0], triangleVertices[2][1]);

    Scalar coordinates[3] = { 0, 0, 0 };
    Triangle_coordinates triangle_coordinates(first_vertex, second_vertex, third_vertex);

    const Point2 query_point = Point2(querryPoint[0], querryPoint[1]);
    triangle_coordinates(query_point, coordinates);

    return tg::vec3(coordinates[0], coordinates[1], coordinates[2]);
}

float
GeometryFunc::computeHeightPointNormal(
    tg::pos3 refPoint,
    tg::vec3 normal,
    tg::pos3 point
)
{
    return -((point.x - refPoint.x) * normal.x + (point.z - refPoint.z) * normal.z) / normal.y + refPoint.y;
}

float
GeometryFunc::computeHeight3Point(
    tg::pos3 refPoint1,
    tg::pos3 refPoint2,
    tg::pos3 refPoint3,
    tg::pos3 point
)
{
    tg::vec3 normal = tg::cross(refPoint2 - refPoint1, refPoint3 - refPoint1);
    return computeHeightPointNormal(refPoint1, normal, point);
}

tg::pos2
GeometryFunc::reflectPointOnLine2D(
    tg::pos2 point,
    tg::vec2 lineVector
)
{
    return tg::pos(2 * (tg::dot(point, lineVector) / tg::dot(lineVector, lineVector)) * lineVector - tg::vec2(point));
}

tg::vec2
GeometryFunc::computeBarycentricCoords(
    tg::pos2 x,
    tg::pos2 i,
    tg::pos2 j,
    tg::pos2 k
)
{
    tg::vec2 v0 = i - k, v1 = j - k, v2 = x - k;
    float den = v0.x * v1.y - v0.y * v1.x;          //cross(v0, v1);
    float u = (v2.x * v1.y - v2.y * v1.x) / den;    //cross(v2, v1) / denominator;
    float v = (v0.x * v2.y - v0.y * v2.x) / den;    //cross(v0, v2) / denominator;

    return tg::vec2(u, v);
}


bool
GeometryFunc::isPointInTriangle2D(
    tg::pos2 pt,
    tg::pos2 v1,
    tg::pos2 v2,
    tg::pos2 v3,
    float eps
)
{
    auto baryCoords = computeBarycentricCoords(pt, v1, v2, v3);
    return (baryCoords.x >= 0.0f - eps &&
            baryCoords.x <= 1.0f + eps &&
            baryCoords.y >= 0.0f - eps &&
            baryCoords.y <= 1.0f + eps &&
            baryCoords.x + baryCoords.y >= 0.0f - eps &&
            baryCoords.x + baryCoords.y <= 1.0f + eps);
}