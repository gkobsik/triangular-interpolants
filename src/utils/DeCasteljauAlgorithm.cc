#include "DeCasteljauAlgorithm.hh"

#include <cassert>

/// ##############
/// ### PUBLIC ###
/// ##############

tg::pos3
DeCasteljauAlgorithm::wrapperTriMeshCT(
    std::shared_ptr<TriMeshCT> mesh,
    pm::halfedge_handle hh,
    tg::vec2 uv,
    int degree,
    int index
)
{
    assert(hh.is_valid());
    
    std::unordered_map<tg::uvec3, TriMeshCT::ControlPoint> indexToCP;
    indexToCP.emplace(std::make_pair(tg::uvec3(3, 0, 0), mesh->S[hh.face()]));
    indexToCP.emplace(std::make_pair(tg::uvec3(2, 1, 0), mesh->I2[hh]));
    indexToCP.emplace(std::make_pair(tg::uvec3(1, 2, 0), mesh->I1[hh]));
    indexToCP.emplace(std::make_pair(tg::uvec3(1, 1, 1), mesh->C[hh]));
    indexToCP.emplace(std::make_pair(tg::uvec3(0, 2, 1), mesh->T[hh]));
    indexToCP.emplace(std::make_pair(tg::uvec3(0, 1, 2), mesh->T[hh.opposite()]));
    indexToCP.emplace(std::make_pair(tg::uvec3(2, 0, 1), mesh->I2[hh.next()]));
    indexToCP.emplace(std::make_pair(tg::uvec3(1, 0, 2), mesh->I1[hh.next()]));
    indexToCP.emplace(std::make_pair(tg::uvec3(0, 3, 0), mesh->V[hh.vertex_from()]));
    indexToCP.emplace(std::make_pair(tg::uvec3(0, 0, 3), mesh->V[hh.vertex_to()]));
    
    assert(indexToCP.size() == 10);
    auto controlPoints = triangleBezierIndizies(degree);
    for (auto pair : indexToCP)
        controlPoints[pair.first] = 
            tg::vec3(pair.second.pos[0], pair.second.val[index], pair.second.pos[1]);

    assert(controlPoints.size() == 10);
    return evaluateTriangle(controlPoints, degree, uv);
    
    return tg::pos3(0, 0, 0);
}

tg::pos3
DeCasteljauAlgorithm::evaluateTriangle(
    std::unordered_map<tg::uvec3, tg::vec3> controlPoints,
    int degree,
    tg::vec2 uv
)
{
    // The degree can be implicitly computed by the number of control points or vice versa
    assert(controlPoints.size() == (degree + 1) * (degree + 2) / 2);
    
    float u = uv[0];
    float v = uv[1];
    float w = 1 - u - v;
    
    tg::uvec3 ei(1, 0, 0);
    tg::uvec3 ej(0, 1, 0);
    tg::uvec3 ek(0, 0, 1);

    auto newControlPoints = triangleBezierIndizies(static_cast<int>(degree - 1));
    for (auto it = newControlPoints.begin(); it != newControlPoints.end(); ++it)
    {
        newControlPoints[it->first] =
            u * controlPoints[it->first + ei] +
            v * controlPoints[it->first + ej] +
            w * controlPoints[it->first + ek];
    }

    if (degree > 1) {
        return evaluateTriangle(newControlPoints, degree - 1, uv);
    }
    else {
        return tg::pos3(newControlPoints.begin()->second);
    }
    
}

/// ###############
/// ### PRIVATE ###
/// ###############

std::unordered_map<tg::uvec3, tg::vec3>
DeCasteljauAlgorithm::triangleBezierIndizies(
    int degree
)
{
    std::unordered_map<tg::uvec3, tg::vec3> indizies;
    for (int i = degree; i >= 0; --i) {
        for (int j = degree - i; j >= 0; --j) {
            auto index = tg::uvec3(i, j, degree - (i + j));
            indizies.emplace(std::make_pair(index, tg::vec3(0, 0, 0) ));
        }
    }
    return indizies;
}
