#pragma once

#include <typed-geometry/tg.hh>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/linear_least_squares_fitting_3.h>
#include <CGAL/Barycentric_coordinates_2/Triangle_coordinates_2.h>

typedef double                      FT;
typedef CGAL::Simple_cartesian<double> Kernel;
typedef CGAL::Simple_cartesian<FT>  K;
typedef K::Line_3                   Line;
typedef K::Plane_3                  Plane;
typedef K::Point_3                  Point;
typedef K::Triangle_3               Triangle;

typedef Kernel::FT                  Scalar;
typedef Kernel::Point_2             Point2;
typedef std::vector<Scalar>         Scalar_vector;
typedef CGAL::Barycentric_coordinates::Triangle_coordinates_2<Kernel> Triangle_coordinates;

class GeometryFunc {
public:
    static tg::vec3
        computeLeastSquaresPlaneNormal(
            const std::vector<tg::pos3>& points
        );

    static tg::vec3
        computePlanarBarycentricCoordinates(
            std::vector<tg::pos2> triangleVertices,
            tg::pos2 querryPoint
        );

    static float
        computeHeightPointNormal(
            tg::pos3 refPoint,
            tg::vec3 normal,
            tg::pos3 point
        );

    static float
        computeHeight3Point(
            tg::pos3 refPoint1,
            tg::pos3 refPoint2,
            tg::pos3 refPoint3,
            tg::pos3 point
        );

    static tg::pos2
        reflectPointOnLine2D(
            tg::pos2 point,
            tg::vec2 lineVector
        );

    static tg::vec2
        computeBarycentricCoords(
            tg::pos2 x,
            tg::pos2 i,
            tg::pos2 j,
            tg::pos2 k
        );

    static bool
        isPointInTriangle2D(
            tg::pos2 pt,
            tg::pos2 v1,
            tg::pos2 v2,
            tg::pos2 v3,
            float eps = 0.0f
        );
};