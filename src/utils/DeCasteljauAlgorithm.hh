#pragma once

#include "geometry/TriMeshCT.hh"

#include "typed-geometry/tg-std.hh"

#include <memory>
#include <unordered_map>

class DeCasteljauAlgorithm {

public:
    static tg::pos3
        wrapperTriMeshCT(
            std::shared_ptr<TriMeshCT> mesh,
            pm::halfedge_handle hh,
            tg::vec2 uv,
            int degree,
            int index);

    //Docu: Hansford, "Handbook of Computer Aided Geometrical Design", 2002
    // - Chapter 4.4.3 The de Casteljau algorithm for Bezier triangles

    
    // evaluates controlPoints with the DeCasteljauAlgorithm - return: evaluated position
    static tg::pos3
        evaluateTriangle(
            std::unordered_map<tg::uvec3, tg::vec3> controlPoints,
            int degree,
            tg::vec2 uv);

    // computes a map with Bezier indizies of the corresponding degree. 
    // values are initialized with (0, 0, 0).
    static std::unordered_map<tg::uvec3, tg::vec3>
        triangleBezierIndizies(
            int degree);
    
};


